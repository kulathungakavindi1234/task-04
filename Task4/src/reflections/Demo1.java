package reflections;

import java.lang.reflect.Field;

public class Demo1 {
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException {
		Simple simple = new Simple(4, 7);
		
		Class<?> cls = simple.getClass();
		
		Field[] fields = cls.getDeclaredFields();
		
		for(Field field: fields) {
			if(field.getName().equals("height")){
				field.setAccessible(true);
				System.out.println(field.getDouble(simple));
			}
		}
	}
}
