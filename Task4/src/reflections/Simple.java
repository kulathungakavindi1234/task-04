package reflections;

public class Simple {
	public double width;
	private double height;
	
	private double result = 0;
	
	public Simple(double width, double height) {
		this.width = width;
		this.height = height;
	}
	
	public void perimeter() {
		result = (width+height)*2;
	}
	
	private void area() {
		result = width*height;
	}
	
	public void setWidth(double width) {
		this.width = width;
	}
	
	private double getWidth() {
		return width;
	}
	
	public void setHieght(double height) {
		this.height = height;
	}
	
	public double getHeight() {
		return height;
	}
	
	private double getAnswer() {
		return result;
	}
	
	public String toString() {
		return String.format("Answer :%.2f", result);
	}
	
}
