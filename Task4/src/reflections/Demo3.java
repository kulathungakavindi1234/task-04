package reflections;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Demo3 {

	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		Simple simple = new Simple(4, 7);

		Class<?> cls = simple.getClass();
		Method[] methods = cls.getDeclaredMethods();
		
		simple.perimeter();
		System.out.println(simple.toString());
		
		for(Method method: methods) {
			if(method.getName().equals("area")) {
				method.setAccessible(true);
				method.invoke(simple);
			}
		}
		
		System.out.println(simple.toString());

	}

}
