package reflections;

import java.lang.reflect.Field;

public class Demo2 {

	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException {
		Simple simple = new Simple(4, 7);

		Class<?> cls = simple.getClass();
		Field[] fields = cls.getDeclaredFields();
		System.out.println(simple.getHeight());

		for (Field field : fields) {
			if (field.getName().equals("height")) {
				field.setAccessible(true);
				field.set(simple,3);
			}
		}
		System.out.println(simple.getHeight());

	}

}
