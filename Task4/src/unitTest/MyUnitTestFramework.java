package unitTest;

import java.lang.reflect.Method;
import java.util.List;
import java.util.ArrayList;

public class MyUnitTestFramework {
	private List<String> results;

	public MyUnitTestFramework() {
		results = new ArrayList<>();
	}

	public void checkMethods(Object object) {
		String methodName;
		String nameFirstPart;
		
		Class<?> cls = object.getClass();
		
		Method[] methods = cls.getDeclaredMethods();
		
		for (Method method : methods) {
			methodName = method.getName();
			nameFirstPart = methodName.substring(0, 5);
			if (nameFirstPart.equals("check")) {
				try {
					method.setAccessible(true);
					method.invoke(object);
					saveResultsSuccess(methodName);
				} catch (Exception e) {
					saveResultsFailure(methodName, e.getCause().getMessage());
				}
			}
		}
		printResults();
	}

	private void saveResultsSuccess(String methodName) {
		results.add("SUCCESS: " + methodName);
	}

	private void saveResultsFailure(String methodName, String error) {
		results.add("FAILURE: " + methodName + " - " + error);
	}

	private void printResults() {
		System.out.println("-Results-");
		for (String result : results) {
			System.out.println(result);
		}
	}

	public static void testEquals(Object expected, Object actual) {
		if (!expected.equals(actual)) {
			throw new AssertionError("Expected: " + expected + "\tActual: " + actual);
		}
	}
	
	public static void testNotEquals(Object value, Object actual) {
		if (value.equals(actual)) {
			throw new AssertionError("Value should not be equal to " + value);
		}
	}
	
	public static void testTrue(boolean val) {
		if(!val) {
			throw new AssertionError("\tExpected: True\tActual: False");
		}
	}
	
	public static void testFalse(boolean val) {
		if(val) {
			throw new AssertionError("\tExpected: False\tActual: True");
		}
	}
	
	public static void testNull(Object object) {
		if(object != null) {
			throw new AssertionError("Expected: Null\tActual: Not Null");
		}
	}
	
	public static void testNotNull(Object object) {
		if(object == null) {
			throw new AssertionError("Expected: Not Null\tActual: Null");
		}
	}
}
