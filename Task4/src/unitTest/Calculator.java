package unitTest;

public class Calculator {
	public double perimeter(double width, double height) {
		return (width+height)*2;
	}
	
	public double area(double width, double height) {
		return width*height;
	}
}
