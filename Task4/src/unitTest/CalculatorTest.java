package unitTest;

public class CalculatorTest {
	Calculator calculator;

	public CalculatorTest() {
		this.calculator = new Calculator();
	}

	public void checkPerimeterTwoxTwoEqualsEight() {
		MyUnitTestFramework.testEquals(8.0, calculator.perimeter(2, 2));
	}

	public void checkAreaThreexFiveEqualsFifteen() {
		MyUnitTestFramework.testTrue(calculator.area(3, 5) == 15);
	}
	
	public void checkPerimeterTenxTwoEqualsTwentyFour() {
		MyUnitTestFramework.testEquals(24.0, calculator.perimeter(10, 2));
	}
	
	public void checkPerimeterSixXThreeNotEqualsTen() {
		MyUnitTestFramework.testNotEquals(10.0, calculator.perimeter(6, 3));
	}

}
